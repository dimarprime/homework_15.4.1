﻿
#include <iostream>
#include <string>

std::string even_or_not;
int number = 0;



int main()
{
	std::cout << "Is i - an even number? Type (true/false):\n";
	std::cin >> even_or_not;
	std::cout << "Count until what positive integer number? Type it down:\n";
	std::cin >> number;
	std::cout << "\n";
	if (even_or_not == "true")
	{
		for (int i = 0; i <= number; i++)
			if (i % 2 == 0)
				std::cout << i << "\n";
	}
	else if (even_or_not == "false")
	{
		for (int i = 0; i <= number; i++)
			if (i % 2 != 0)
				std::cout << i << "\n";
	}

}
